if exists('g:fvim_loaded')
  " good old 'set guifont' compatibility
  if Platform('linux') 
    set guifont=Fira\ Code:h10
  elseif Platform('windows')
    set guifont=Courier\ New:h16
  endif
  " set guifont=FiraCode\ Nerd\ Font\ Mono:h10
  " Ctrl-ScrollWheel for zooming in/out
  nnoremap <silent> <C-ScrollWheelUp> :set guifont=+<CR>
  nnoremap <silent> <C-ScrollWheelDown> :set guifont=-<CR>
  nnoremap <A-CR> :FVimToggleFullScreen<CR>
  " Some fancy Cursor effects
  FVimCursorSmoothMove v:true
  FVimCursorSmoothBlink v:true

  " Background composition
  " FVimBackgroundImage '~/Pictures/Myriacore - Extended/wall-16x9.jpg'
  " FVimBackgroundComposition 'acrylic'
  " FVimBackgroundOpacity 0.85
  " FVimBackgroundAltOpacity 0.55
  " FVimBackgroundImageVAlign 'center'
  " FVimBackgroundImageHAlign 'center'
  " FVimBackgroundImageStretch 'uniform' 
  " FVimBackgroundImageOpacity 0.85
  " FVimBackgroundComposition 'acrylic'   " 'none', 'blur' or 'acrylic'
  " FVimBackgroundOpacity 0.85            " value between 0 and 1, default bg opacity.
  " FVimBackgroundAltOpacity 0.85         " value between 0 and 1, non-default bg opacity.
  " FVimBackgroundImageVAlign 'center'    " vertial position, 'top', 'center' or 'bottom'
  " FVimBackgroundImageHAlign 'center'    " horizontal position, 'left', 'center' or 'right'
  " " FVimBackgroundImageStretch 'fill'     " 'none', 'fill', 'uniform', 'uniformfill'
  " FVimBackgroundImageOpacity 0.85       " value between 0 and 1, bg image opacity
endif

